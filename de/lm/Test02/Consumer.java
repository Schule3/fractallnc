package de.lm.Test02;

import java.util.concurrent.BlockingQueue;


public class Consumer implements Runnable{
	private final BlockingQueue<mendelPar> queue;
	private Mandelbrot mb = new Mandelbrot();
	private String name = "";
	public Consumer(BlockingQueue<mendelPar> queue, String name) {
		this.queue = queue;
		this.name = name;
	}
	@Override
	public void run() {
		try {
            while (queue.size() > 0) {
                mendelPar mp = queue.take();
                process(mp);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
	}
	public void process(mendelPar mp) {
		if(mp.ymit == 0) {
			mb.computFractalHalf(mp);
		}else {
			mb.computFractal(mp);
		}
		System.out.println("Thread "+name+ " fertig: " + mp.name);
	}
	
}