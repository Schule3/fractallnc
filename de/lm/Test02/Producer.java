package de.lm.Test02;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import de.lm.frac.ColorCalculators.AudioBlueColorCalculator;
import de.lm.frac.ColorCalculators.AudioColorMix1ColorCalculator;
import de.lm.frac.ColorCalculators.AudioColorMix2ColorCalculator;
import de.lm.frac.ColorCalculators.AudioColorMix3ColorCalculator;
import de.lm.frac.ColorCalculators.AudioColorMix4ColorCalculator;
import de.lm.frac.ColorCalculators.AudioRedColorCalculator;
import de.lm.frac.ColorCalculators.BlackColorCalculator;
import de.lm.frac.ColorCalculators.BlueColorCalculator;
import de.lm.frac.ColorCalculators.ColorCalculator;
import de.lm.frac.ColorCalculators.RedColorCalculator;
import de.lm.frac.ColorCalculators.YellowColorCalculator;

public class Producer implements Runnable{
	private final BlockingQueue<mendelPar> queue;
	public Producer(BlockingQueue<mendelPar> queue) {
        this.queue = queue;
    }
	public void process() throws InterruptedException, IOException {
		BufferedReader reader = new BufferedReader(new FileReader("level.txt"));
		String line = reader.readLine();			
		System.out.println("Schreibe Puffer");
		Mandelbrot m = new Mandelbrot();
		int height = 1080;
		int width = 1920;
		double zoom = 0; //0.000000001;//0.005;//
		double xmit = 0;//-1.766564;
		double ymit = 0;//-0.0419999;
		String name = "";
		int ind = 0;
		double zooma = 0;
		double zoomb = 0;
		double xmita = 0;
		double xmitb = 0;
		double ymita = 0;
		double ymitb = 0;
		
		//Initiale Einstellungen
		zoom = 1;
		xmit = 0;
		ymit = 0;
		ColorCalculator cc = null;
		while (line != null) {
			line = line.split(",")[1].replace(" ", "");
			name = "frac/"+toStr(ind)+ "frac.png";
			ind++;
			while(queue.size() > 90) {
				Thread.sleep(100);
			}
			if(ind == 0) {
				zoom = 0;
				//xmit = xmit;
				//ymit = ymit;
				cc = new BlueColorCalculator();
			}else if(ind < 20) {
				zoom = zoom - ((1-0.01)/19);
				//xmit = xmit;
				//ymit = ymit;
				cc = new BlueColorCalculator();
			}else if(ind <40) {
				zoom = zoom - ((0.01-0.001)/19);
				xmit = xmit;
				ymit = ymit;
				cc = new RedColorCalculator();
			}else if(ind <60) {
				zoom = zoom - ((0.001-0.002)/19);
				xmit = xmit - ((0 + 1.5)/19);
				ymit = ymit;
				cc = new YellowColorCalculator();
			}else if(ind <80) {
				zoom = zoom;
				xmit = xmit;
				ymit = ymit;
				cc = new BlackColorCalculator();
				zooma = zoom;
			}else if(ind <205) {
				zoom = zoom - ((zooma-0.00002)/125);
				xmita = xmit;
				ymit = ymit;
				zoomb = zoom;
				cc = new AudioBlueColorCalculator();
			}else if(ind <405) {//fertig
				
				zoom = zoom*0.9998;//zoom - ((zoomb-0.0000008)/200);
				xmit = xmit *1.001;//- (xmita - 1.71)/200;
				xmitb = xmit;
				zooma = zoom;
				cc = new AudioRedColorCalculator();
			}else if(ind == 405){
				xmita= xmit;
				ymitb = ymit;
			}else if(ind <605) {//Todo
				zoom = zoom * 1.01;//zoom - ((zooma-0.0000000008)/600);
				xmit = xmit + (-0.74455-xmita)/200; //xmit - (1.766564 - xmitb)/600;
				ymit = ymit + (-0.1419899990-ymita)/200;//ymit - (ymitb - 0.0419999)/600;
				//ymita = ymit;
				//zoomb = zoom;
				cc = new AudioBlueColorCalculator();
			}/*else if(ind <2005) {//TODO
				
				zoom = zoom - ((zoomb-0.0000000008)/1000);
				xmit = xmit - (1.71 -0.74455)/1200;
				ymit = ymit - (ymita -0.141989999)/1000;
				zooma = zoom;
				cc = new AudioColorMix3ColorCalculator();
			}*/else if(ind <3005) {//fertig
				xmit = -0.74455;//-1.766564;
				ymit = -0.141989999;
				zoom = zoom *0.95;//zoom- ((zooma-0.00000000009)/1010);
				zoomb = zoom;
				cc = new AudioColorMix4ColorCalculator();
			}else {
				reader.close();
			}
			/*double zoom = 0.0000000001;//0.000000001;
		double xmit = -0.74455;//-1.766564;
		double ymit = -0.141989999;*/
			
			if(ind < 400) {
			}else {
				queue.put(new mendelPar(height, width, zoom, xmit, ymit, name, Integer.valueOf(line), cc));
			}
			System.out.println("Add: " + ind);
			line = reader.readLine();
		}
		reader.close();
		System.out.println(xmit);
		System.out.println(ymit);
		System.out.println(zoom);
		reader.close();
		//m.computFractal(height, width, zoom, xmit, ymit, "fraczoom4.png",3);
		System.out.println("fertig !!!!!!");
	}
	static String toStr(int i) {
		String s = ""+i;
		while(s.length()<10) {
			s = "0"+s;
		}
		return s;
	}
	@Override
	public void run() {
		try {
            try {
				process();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
		
	}
}
/*
 * 0.005
 * 0
 * 0
 * 
 * 
 * 0.002
 * -1
 * 0
 * 
 * 
 *#0.0008
 * -1.6
 * 0
 * 
 * 
 * 0.00008
 * -1.71
 * 0
 * 
 * 
 * 0.000008
 * -1.71
 * 0
 */