package de.lm.Test02;

public class Complex {
	double re;
	double im;
	public Complex(double real, double imag) {
		this.re = real;
		this.im = imag;
	}
	public Complex() {
		this.re = 0;
		this.im = 0;
	}
	public void setComplex(double real, double imag) {
		this.re = real;
		this.im = imag;
	}
	public double getReal() {
		return this.re;
	}
	public double getImag() {
		return this.im;
	}
	public String toString() {
		return "" + re + " + " + im +" i";
		
	}
	public Complex add(Complex c) {
		double x = this.re + c.getReal();
		double y = this.im + c.getImag();
		return new Complex(x,y);
	}
	public Complex sub(Complex c) {
		double x = this.re - c.getReal();
		double y = this.im - c.getImag();
		return new Complex(x,y);
	}
	public Complex mult(Complex c) {
		double x = this.re * c.getReal() - this.im * c.getImag();
		double y = this.re * c.getImag() + this.im * c.getReal();
		return new Complex(x,y);
	}
	public double abs() {
		return Math.hypot(this.re, this.im);
	}
}
