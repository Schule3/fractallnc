package de.lm.Test02;

import de.lm.frac.ColorCalculators.ColorCalculator;
import de.lm.frac.ColorCalculators.ColorCalculatorImplementation;

public class Mandelbrot {
	public int mandelbrotCalculation(Complex c) {
		Complex z = new Complex(0,0);
		int i = 0;
		while( i < 255 && z.abs() < 2) {
			z = z.mult(z);
			z = z.add(c);
			//System.out.println(z);
			i += 1;
		}
		return i;
	}
	public byte[] indexToColor(byte[] pic,int index, int color, int audio, ColorCalculator cc) {
		pic[index+0] = (byte) ((byte) (cc.color1(color, audio))%256);
		pic[index+1] = (byte) ((byte) (cc.color2(color, audio))%256);
		pic[index+2] = (byte) ((byte) (cc.color3(color, audio))%256);
		return pic;
	}
	public byte[] indexToColor(byte[] pic, int index, int color, int como, int audio) {
		if(como==1) {
			pic[index+0] = (byte) ((color*1)%256);
			pic[index+1] = (byte) ((color*1)%256);
			pic[index+2] = (byte) ((color*1)%256);
			return pic;
		}else if(como == 2){
			pic[index+0] = (byte) ((color*color*1)%256);
			pic[index+1] = (byte) ((color*3)%256);
			pic[index+2] = (byte) ((color*25)%256);
			return pic;
		}else if(como == 3){
			pic[index+0] = (byte) ((color*color*1)%256);
			pic[index+1] = (byte) ((color*3)%256);
			pic[index+2] = (byte) ((color*color*25)%256);
			return pic;
		}else if(como == 4){
			pic[index+0] = (byte) ((color*color*1)%256);
			pic[index+1] = (byte) ((color*3)%256);
			pic[index+2] = (byte) ((color*color*color*25)%256);
			return pic;
		}else if(como == 5){
			pic[index+0] = (byte) ((color*1)%256);
			pic[index+1] = (byte) ((color*3)%256);
			pic[index+2] = (byte) ((color*color*color*25)%256);
			return pic;
		}else if(como == 6){
			pic[index+0] = (byte) ((color*1)%256);
			pic[index+1] = (byte) ((color*3)%256);
			pic[index+2] = (byte) ((color*25)%256);
			return pic;
		}else if(como == 12){
			pic[index+0] = (byte) ((color*color+audio/100*1)%256);
			pic[index+1] = (byte) ((color*3)%256);
			pic[index+2] = (byte) ((color*25)%256);
			return pic;
		}
		pic[index+0] = (byte) ((color*color*1)%256);
		pic[index+1] = (byte) ((color*3)%256);
		pic[index+2] = (byte) ((color*color*color*25)%256);
		return pic;
	}
	public void computFractalHalf(mendelPar mp) {
		computFractalHalf(mp.height, mp.width, mp.zoom, mp.xmit, mp.ymit, mp.name, mp.audio, mp.cc);
	}
	public void computFractal(mendelPar mp) {
		
		computFractal(mp.height, mp.width, mp.zoom, mp.xmit, mp.ymit, mp.name, mp.audio, mp.cc);
	}
	public void computFractal(int height, int width, double zoom, double xmit, double ymit, String name, ColorCalculator cc) {
		computFractal(height, width, zoom, xmit, ymit, name,  -1, cc); 
	}
	public void computFractalHalf(int height, int width, double zoom, double xmit, double ymit, String name, ColorCalculator cc) {
		computFractalHalf(height, width, zoom, xmit, ymit, name,  -1, cc); 
	}
	public void computFractal(int height, int width, double zoom, double xmit, double ymit, String name, int audio, ColorCalculator cc) {
		
		
		byte[] picture = new byte[width*height*3];
		
		double xmin = xmit - width / 2 * zoom;
		double ymin = ymit - height / 2 * zoom;
		
		int i = 0;
		for(int y= 0; y <height; y++) {
			for(int x = 0; x < width; x++) {
				Complex c = new Complex(xmin + x * zoom, ymin + y * zoom);
				
				int color = mandelbrotCalculation(c);
				picture = indexToColor(picture, (y*width+x)*3,color, audio, cc);
				i++;
			}
		}
		new PNG().writePNG(name, picture, height, width);
	}
	public void computFractalHalf(int height, int width, double zoom, double xmit, double ymit, String name, int audio, ColorCalculator cc) {
		
		
		byte[] picture = new byte[width*height*3];
		
		double xmin = xmit - width / 2 * zoom;
		double ymin = ymit - height / 2 * zoom;
		
		//int i = 0;
		for(int y= 0; y <(height/2); y++) {
			for(int x = 0; x < width; x++) {
				Complex c = new Complex(xmin + x * zoom, ymin + y * zoom);
				
				int color = mandelbrotCalculation(c);
				picture = indexToColor(picture, (y*width+x)*3,color, audio, cc);
				picture = indexToColor(picture, ((height-y-1)*width+x)*3,color, audio, cc);
				//i++;
			}
		}
		new PNG().writePNG(name, picture, height, width);
	}
	public static void main(String[] args) {
		System.out.println("start");
		Mandelbrot m = new Mandelbrot();
		int height = 1080;
		int width = 1920;
		double zoom = 0.0000000001;//0.000000001;
		double xmit = -0.74455;//-1.766564;
		double ymit = -0.141989999;
		String name = zoom+"_"+xmit+"_"+ymit+"_"+"frac.png";
		m.computFractal(height, width, zoom, xmit, ymit, name,1,new ColorCalculatorImplementation());
		System.out.println("stop");
	}
}
