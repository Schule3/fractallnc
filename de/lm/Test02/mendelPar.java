package de.lm.Test02;

import de.lm.frac.ColorCalculators.ColorCalculator;

public class mendelPar {
	public int height;
	public int width;
	public double zoom;
	public double xmit;
	public double ymit;
	public String name;
	public ColorCalculator cc;
	public int audio;

	public mendelPar(int height, int width, double zoom, double xmit, double ymit, String name, ColorCalculator cc) {
		this.height = height;
		this.width = width;
		this.zoom = zoom;
		this.xmit = xmit;
		this.ymit = ymit;
		this.name = name;
		this.cc = cc;
		this.audio = -1;
	}
	public mendelPar(int height, int width, double zoom, double xmit, double ymit, String name, int audio, ColorCalculator cc) {
		this.height = height;
		this.width = width;
		this.zoom = zoom;
		this.xmit = xmit;
		this.ymit = ymit;
		this.name = name;
		this.cc = cc;
		this.audio = audio;
	}
}
