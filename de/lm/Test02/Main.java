package de.lm.Test02;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
	public static void main(String[] args) throws InterruptedException {

        BlockingQueue<mendelPar> queue = new LinkedBlockingQueue<>(100);

        new Thread(new Producer(queue)).start();
        Thread.sleep(1000);
        new Thread(new Consumer(queue,"1")).start();
        new Thread(new Consumer(queue,"2")).start();
        new Thread(new Consumer(queue,"3")).start();
        new Thread(new Consumer(queue,"4")).start();
        new Thread(new Consumer(queue,"5")).start();
        new Thread(new Consumer(queue,"6")).start();
        new Thread(new Consumer(queue,"7")).start();
        new Thread(new Consumer(queue,"8")).start();
        new Thread(new Consumer(queue,"9")).start();
        new Thread(new Consumer(queue,"10")).start();
        new Thread(new Consumer(queue,"11")).start();
        new Thread(new Consumer(queue,"12")).start();

    }
}
