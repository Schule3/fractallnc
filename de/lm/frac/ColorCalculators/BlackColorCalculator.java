package de.lm.frac.ColorCalculators;

public class BlackColorCalculator implements ColorCalculator {
	@Override
	public byte color1(int c, int audio) {
		return (byte)  (c*1%256);
	}
	@Override
	public byte color2(int c, int audio) {
		return (byte)  (c*1%256);
	}
	@Override
	public byte color3(int c, int audio) {
		return (byte)  (c*1%256);
	}
}