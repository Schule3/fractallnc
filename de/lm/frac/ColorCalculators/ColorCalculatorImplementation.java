package de.lm.frac.ColorCalculators;

public class ColorCalculatorImplementation implements ColorCalculator {
	@Override
	public byte color1(int c, int audio) {
		return (byte)  (c%256);
	}
	@Override
	public byte color2(int c, int audio) {
		return (byte)  (c%256);
	}
	@Override
	public byte color3(int c, int audio) {
		return (byte)  (c%256);
	}
}