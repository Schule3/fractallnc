package de.lm.frac.ColorCalculators;

public interface ColorCalculator {
	public byte color1(int c,int audio);
	public byte color2(int c, int audio);
	public byte color3(int c, int audio);
}
