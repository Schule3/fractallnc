package de.lm.frac.ColorCalculators;

public class AudioColorMix1ColorCalculator implements ColorCalculator {
	@Override
	public byte color1(int c, int audio) {
		return (byte)  ((c*c*audio/300*1)%256);
	}
	@Override
	public byte color2(int c, int audio) {
		return (byte)  ((c*3)%256);
	}
	@Override
	public byte color3(int c, int audio) {
		return (byte)  ((c*25)%256);
	}
}