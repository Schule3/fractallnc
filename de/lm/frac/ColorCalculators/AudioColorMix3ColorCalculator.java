package de.lm.frac.ColorCalculators;

public class AudioColorMix3ColorCalculator implements ColorCalculator {
	@Override
	public byte color1(int c, int audio) {
		return (byte)  ((c*c*audio/100*1)%256);
	}
	@Override
	public byte color2(int c, int audio) {
		return (byte)  ((c*c*audio/100*3)%256);
	}
	@Override
	public byte color3(int c, int audio) {
		return (byte)  ((c*25)%256);
	}
}