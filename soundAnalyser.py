import pyaudio
import wave
import sys
import numpy as np
#import fractal2 as fr

CHUNK = 1411*1000#2**11#1024

if len(sys.argv) < 2:
    print("Plays a wave file.\n\nUsage: %s filename.wav" % sys.argv[0])
    sys.exit(-1)

wf = wave.open(sys.argv[1], 'rb')

# FPS
fps = 25

# Brechne Lied länge
len = (wf.getnframes() / wf.getframerate())
# berechne frames
framesize = int(wf.getframerate()/fps)
# Setze Pointer zurück
wf.rewind()

p = pyaudio.PyAudio()

for i in range(int(fps*len)):
	data = wf.readframes(framesize)
	data1=np.fromstring(data,dtype=np.int16)
	peak=np.average(np.abs(data1))*2
	bars="#"*int(50*peak/2**16)
	print("%04d, %05d, %s"%(i,peak,bars))

p.terminate()
