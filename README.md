# Quick and dirty Fractal implementation

### Die Python variante wurde in Java umgeschrieben und optimiert.

optimierungen:
- eingene Farbprofile nutzbar (diese können den Audio vu peak verwenden)
- doppelt so schnell bei Bilder welche den Mittelpunkt der y Achse bei 0 haben.
- Implementation von Multithreading (quick and dirty variante)



### How To:

Python3 vu Analyser -> analysiert die Lautstärke der Musik 

python3 soundAnalyser.py Musik.mp3 > level.txt

Java erzeugt die Bild (da performanter als python inklusive Multithreading)

kompilieren
(Main.java liegt in de.lm.Main.java)

Main.class ausführen. (benötigt die level.txt als eingabe)